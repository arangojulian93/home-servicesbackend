<?php

require_once 'vendor/autoload.php';

$app = new \Slim\Slim();

$db = new mysqli('localhost', 'root', '', 'Home_services');



//Configuración de cabeceras
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

//Listar Todas las facturas

$app->get("/facturas", function() use($app, $db){

$sql= 'SELECT * FROM facturas ORDER BY id DESC;';
$query = $db->query($sql);
 

$facturas =  array();
while ($factura = $query->fetch_assoc() ) {
	$facturas[]= $factura;
}
 
 	$result =  array(
 		'status' => 'success' , 
        'code'=> 200,
        'data'=> $facturas
 	);


echo json_encode($result); 

});

//Devolver una sola factura

$app->get('/factura/:id', function($id) use($app, $db){

	 $sql= 'SELECT * FROM facturas WHERE id = '.$id;

	 $query = $db->query($sql);


      $result = array(
      	'status'    => "error", 
        'code'      => 404,
        'message'   => 'factura no disponible'

      );
	 if($query->num_rows == 1){

	 	$factura = $query->fetch_assoc();

	 	$result = array(
      	'status'    => "success", 
        'code'      => 200,
        'data'   => $factura

      );

	 }

    echo json_encode($result);

});


//Actualizar una factura

$app->post('/update-factura/:id', function($id) use($app, $db){

  $json = $app->request->post('json');
  $data= json_decode($json, true);

  $sql = "UPDATE facturas SET ".

   "direccion = '{$data["direccion"]}'," .
   "nameSede = '{$data["nameSede"]}',";


   $sql .= "costo = '{$data["costo"]}' WHERE id = {$id}";

     $query = $db->query($sql);

     if($query){
 	    
 	    $result = array(
      	'status'    => "success", 
        'code'      => 200,
        'message'   => 'Factura Actualizado Correctamente!!'

      );

     }else{

     	$result = array(
      	'status'    => "error", 
        'code'      => 404,
        'message'   => 'La Factura no se ha Actualizado'

      );

     }

      echo json_encode($result);

});



// Guardar Factura

$app->post('/Facturas', function() use($app, $db){

$json = $app->request->post('json');
$data= json_decode($json, true);

 //var_dump($json);
 //var_dump($data);



if(!isset($data['nombre'])){
	$data['nombre']=null;
}
if(!isset($data['direccion'])){
	$data['direccion']=null;
}
if(!isset($data['servicio'])){
	$data['servicio']=null;
}
if(!isset($data['nameSede'])){
	$data['nameSede']=null;
}
if(!isset($data['costo'])){
	$data['costo']=null;
}

 $query= "INSERT INTO facturas VALUES(NULL,".
        "'{$data['nombre']}',".
        "'{$data['direccion']}',".
        "'{$data['servicio']}',".
        "'{$data['nameSede']}',".
        "'{$data['costo']}'".    
");";


$insert=$db->query($query);

 	$result =  array(
 		'status' => 'error' , 
        'code'=> 404,
        'message'=> 'Factura No se a creado '
 	);

 if($insert){
 	$result =  array(
 		'status' => 'success' , 
        'code'=> 200,
        'message'=> 'Factura creada correctamente'
 	);
 }

echo json_encode($result);




});


$app->run();