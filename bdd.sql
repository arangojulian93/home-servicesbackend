CREATE DATABASE IF NOT EXISTS Home_services;

USE Home_services;

CREATE TABLE facturas(

id int(255) auto_increment  not null,
nombre varchar(255),
direccion varchar(255),
servicio varchar(255),
nameSede varchar(255),
costo varchar(255),

CONSTRAINT pk_facturas PRIMARY KEY(id)

)ENGINE=InnoDb;